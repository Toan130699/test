import re
def calculate(number_one, number_two, operator):
	number_one = number_one.replace(',', '.')
	number_two = number_two.replace(',','.')
	operator = operator.strip()
	if operator == '+':
		result = float(number_one) + float(number_two)	
	elif operator == '-':
		result = float(number_one) + float(number_two)
	elif operator == '*':
		result = float(number_one) * float(number_two)
	elif operator == '/':
		try:
			result = float(number_one) / float(number_two)
		except ZeroDivisionError:
			result = 'Error'

	result = str(result)
	if result[-2:]=='.0':
		result = str(int(float(result)))

	return result

def excute_path(path):
	try:
		f = open(path, "r")
		result = f.read()
	except FileNotFoundError:
		result = 'None'
	return result

def option_1():
	number_one = input('Input the first number: ')
	number_two = input('Input the second number: ')
	while True:
		operator = input('Input the operator: ')
		if operator in '/ * + -':
			break
		else:
			print('Wrong type of operator. Try aigain.')
			pass
	result = calculate(number_one, number_two, operator)
	# print(number_one, ' ', operator, ' ', number_two, ' = ', result)
	print("Result: ", result)

def option_2():
	while True:
		path = input('Input the absolute path to file (Input.txt): ')
		print(path)
		test = excute_path(path)
		if test != 'None':
			break
		else:
			print('File was not Found')
			pass

	lst_data = test.split('\n')
	data_output = ""
	for data in lst_data:
		if len(data) != 0:
			lst_number = re.findall(r'[-+]?\d*[,.]?\d+|\d+', data)
			operator = re.findall(r'/|\*|\+|-', data)
			result = calculate(lst_number[0], lst_number[1], operator[0])
			data_output = data_output + lst_number[0] + " " + operator[0] + " " + lst_number[1] + " = " + result + "\n" 
		else:
			pass
	f = open(r"D:\output.txt", "a")
	f.write(data_output)
	f.close()



def end_question():
	while True:
		question = input('Do you want to continue to use this program (Yes/No): ')
		question = question.lower()
		if question == 'yes' or question == 'no':
			break
		else:
			pass
	return question

if __name__ == "__main__":
	while True:
		print('1/ Input two numbers, operators (+, -, *, / ) va print the result via console.')
		print('2/ Read the input.txt to progress and print the result to output.txt')
		while True:
			option = input('Your choice of number (1 or 2): ')
			if option in "1 2":
				break
			else:
				print("Your choice is wrong number. Try again.")
				pass

		if option == "1":
			option_1()
		elif option == "2":
			option_2()

		answer = end_question()
		if answer == 'yes':
			pass
		elif answer == 'no':
			break
			
		



