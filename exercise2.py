import re
import os
from exercise1 import end_question

if __name__ == "__main__":
	while True:
		ip_address = input('Input IPv4 address: ')
		lst_number = re.findall(r'\d+', ip_address)
		lst_dots = re.findall(r'\.', ip_address)
		if len(lst_number) == 4 and len(lst_dots) == 3:
			check = ''
			for number in lst_number:
				if number[0] == '0' or int(number) > 255:
					check = 'Invalid IPv4 address'
					break
				else:
					pass
			if len(check) == 0:
				print("Valid IPv4 address")
			else:
				print(check)

		else:
			print('here')
			print('Invalid IPv4 address')

		answer = end_question()
		if answer == 'yes':
			pass
		elif answer == 'no':
			break




