import re

def sort_1(lst_number):
	n = len(lst_number)
	for i in range(n-1):
		for j in range(0, n-i-1):
			if lst_number[j] > lst_number[j+1]:
				lst_number[j], lst_number[j+1] = lst_number[j+1], lst_number[j]
	return lst_number

def sort_2(lst_number):
	for i in range(len(lst_number)):
		min_index = i
		for j in range(i+1, len(lst_number)):
			if lst_number[min_index] > lst_number[j]:
				min_index = j
		lst_number[i], lst_number[min_index] = lst_number[min_index], lst_number[i]
	return lst_number

if __name__ == "__main__":
	# while True:
	# 	str_lst_number = input('List number: ')
	# 	lst_number = re.findall(r'[-+]?\d*[,.]?\d+|\d+', str_lst_number)
	# 	lst_number_ = []
	# 	for number in lst_number:
	# 		number = number.replace(',', '.')
	# 		lst_number_.append(float(number))
	lst_number = [3 ,-1 , 2 , 10 , 0.2 , 20 , 40]
	print(sort_1(lst_number))
	print(sort_2(lst_number))