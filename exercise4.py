import re
from exercise1 import end_question

class ManageEployees(object):
	"""docstring for ManageEployees"""
	def __init__(self, name="", sexual="", birthday="", salary=""):
		self.name = name
		self.sexual = sexual
		self.birthday = birthday
		self.salary = salary

	def printData(self):
		return (self.name, self.sexual, self.birthday, self.salary)
# Dùng để xác thực ngày tháng có đúng cấu trúc?
def check_day(days):
	lst_number = re.findall(r'\d+', days)
	if len(lst_number) == 3:
		if len(lst_number[0]) == 2 and len(lst_number[1]) == 2 and len(lst_number[2]) == 4:
			lst_month_30 = [4, 6, 9, 11]
			month = int(lst_number[1])
			day = int(lst_number[0])
			year = int(lst_number[2])
			if month == 2:
				if (year % 400 == 0) or ((year % 4 == 0) and (year % 100 != 0)):
					if day <= 28:
						result = "Right"
					else:
						result = "Failed"
				else:
					if day <= 27:
						result = 'Right'
					else:
						result = 'Failed'
			else:							
				if month > 0 and month <= 12:
					try:
						test = lst_month_30.index(month)
						if day <= 30:
							result = "Right"
						else:
							result = 'Failed'
					except:
						if day <= 31:
							result= 'Right'
						else:
							result= 'Failed'
				else:
					result = "Failed"
		else:
			result = "Failed"
	else:
		result = "Failed"
	return result

# Input Data
def inputData():
	name = input('Name: ')

	while True:
		sexual = input('Sexual (Male/Female): ')
		if re.search(r'male|female', sexual.lower()):
			break
		else:
			print('Filled wrong. Try again.')
			pass

	while True:
		birthday = input('Birthday (dd/mm/yyyy): ')
		check = check_day(birthday)
		if check != "Failed":
			break
		else:
			pass
		
	salary = input('Salary: ')
	data = "- " + name + " - " + sexual + ' - ' + birthday + ' - ' + salary
	return data

#Lấy dữ liệu ra từ Input_exercise.txt
def getDatafromDatabase():
	f = open('Input_exercise4.txt', 'r')
	text = f.read()
	f.close()

	employees = []
	lst_data = text.split('\n')
	for data in lst_data:
		if len(data) > 0:
			data_= data.split('-')
			employees.append(ManageEployees(data_[1], data_[2], data_[3], data_[4]))
		else:
			pass
	return employees

def inputDatatoDatabase(lst_employees):
	data = ""
	for employee in lst_employees:
		data = data + '\n'+ "- " + employee.name + " - " + employee.sexual + ' - ' + employee.birthday + ' - ' + employee.salary
	f = open('test_output.txt', 'w')
	f.write(data)
	f.close()


# Chỉnh sửa thông tin
def doEdit():
	keywords = input('Keywords: ')
	keywords = keywords.lower()
	lst_employees = getDatafromDatabase()
	lst_employees_ = []
	for employee in lst_employees:
		test = re.search(keywords, employee.name.strip().lower())
		if test != None:
			lst_employees_.append(employee)

	i = 0
	for employee in lst_employees_:
		i += 1
		print(i,".",employee.name.strip(), " - ", employee.sexual.strip())
	while True:	
		option = input('Select ID you want to edit: ')
		if int(option) - 1 <= len(lst_employees_):
			break
		else:
			pass
	id_user = int(option) -1
	employee_edit = lst_employees_[id_user]
	print('1. Name')
	print('2. Sexual')
	print('3. Birthday')
	print('4. Salary')
	option_id = input('Your choice of number (1 or 2 or 3 or 4): ')

	if option_id == "1":
		text = input('New name: ')
		new_ = ManageEployees(text, employee_edit.sexual, employee_edit.birthday, employee_edit.salary)

	elif option_id == "2":
		text = input('New Sexual: ')
		while True:
			text = input('Sexual (Male/Female): ')
			if re.search(r'male|female', text.lower()):
				break
			else:
				print('Filled wrong. Try again.')
				pass
		new_ = ManageEployees(employee_edit.name, text, employee_edit.birthday, employee_edit.salary)

	elif option_id == "3":
		text = input('New Birthday: ')
		while True:
			text = input('Birthday (dd/mm/yyyy): ')
			check = check_day(text)
			if check != "Failed":
				break
			else:
				pass
		new_ = ManageEployees(employee_edit.name, employee_edit.sexual, text, employee_edit.salary)

	elif option_id == "4":
		text = input('New Salary: ')
		new_ = ManageEployees(employee_edit.name, employee_edit.sexual, employee_edit.birthday, text)
	lst_employees.remove(employee_edit)
	lst_employees.append(new_)

	for employee in lst_employees:
		print(employee.printData())
	inputDatatoDatabase(lst_employees)

# Tìm và xóa theo tên
def doDelete():
	keywords = input('Keywords: ')
	keywords = keywords.lower()
	lst_employees = getDatafromDatabase()
	lst_employees_ = []
	for employee in lst_employees:
		test = re.search(keywords, employee.name.strip().lower())
		if test != None:
			lst_employees_.append(employee)

	i = 0
	for employee in lst_employees_:
		i += 1
		print(i,".",employee.name.strip(), " - ", employee.sexual.strip())
	option = input("Do you want to delete all that (yes/no): ")
	if option.lower() == "yes":
		for employee in lst_employees_:
			lst_employees.remove(employee)
	elif option.lower() == "no":
		id_ = input('Select ID you want to delete: ')
		lst_number = re.findall(r'\d+', id_)
		lst_number = tuple(lst_number)
		for number in lst_number:
			lst_employees.remove(lst_employees_[int(number)-1])

	for employee in lst_employees:
		print(employee.printData())
	inputDatatoDatabase(lst_employees)
	# return lst_employees

if __name__ == "__main__":
	while True:
		print('1. Add employess.')
		print('2. Edit employess.')
		print('3. Remove employess.')
		while True:
			option = input('Your choice of number (1 or 2 or 3): ')
			if option in '1 2 3':
				break
			else:
				print("Your choice is wrong number. Try again.")
				pass
		if option == "1":
			data_ = inputData()
			data = "\n" + data_
			f = open('Input_exercise4.txt', 'a')
			data = f.write(data)
			f.close()
		elif option == "2":
			doEdit()
		elif option == '3':
			doDelete()
		answer = end_question()
		if answer == 'yes':
			pass
		elif answer == 'no':
			break
	# a = ManageEployees()
	# b = a.inputData()
	# print(a.printData())